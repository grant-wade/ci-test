
fn main() {
    println!("{}", get_string());
    println!("Testing");
    testing_function();
}


/// Get the string "Hello, World!"
/// 
/// ```
/// let string = get_string();
/// assert_eq("Hello, World!", string);
/// ```
fn get_string() -> String {
    format!("Hello, {}!", "World")
}

fn testing_function() {
    println!("Test");
}

#[test]
fn test_it_all() {
    assert_eq!(1, 1);
    assert_eq!(2, 2);
}

#[test]
fn test_get_string() {
    let string = get_string();

    assert_eq!(string, "Hello, World!");
}

// #[test]
// fn test_will_fail() {
//     assert_eq!(1, 2);
// }
